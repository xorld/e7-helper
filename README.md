<div align="center">
<h1>E7Helper</h1>
<p>一款多功能覆盖的第七史诗脚本助手</a> </p>

<p>
    <a href=https://boluokk.gitee.io/e7-helper/>使用文档(必读)</a>
    <a href=https://qm.qq.com/cgi-bin/qm/qr?k=o6MW-K-Ws6A2-S_WhHEro1JggzREWPzt&jump_from=webapi&authKey=PRSHw0kOjKtnqEVwyXCRHnKKwAWhzXWD/y486deoyZ/AWyNfGLwHIEwjb8gf9yoX>qq交流群:206490280</a>
</p>

![](https://img.shields.io/badge/script_size-32KB-blue)
![](https://img.shields.io/badge/downloads-1k+-green)
![](https://img.shields.io/badge/repo_size-3.54MB-orange)
![](https://img.shields.io/badge/total_line-3k+-purple)

![](cover.png)

</div>

### 开发
1、下载懒人精灵IDE(3.8.6.2)

2、编码调试
```python
1、将整个项目 clone 下来
2、填写 start.py 中的 projectPath 和 packagePath
3、运行 copy() 函数
4、开始编码调试
```

3、发布
```python
1、在懒人IDE中打包脚本文件
2、运行 start.py 中的 saveAndPush()函数
```

[//]: # (4、开发教程)
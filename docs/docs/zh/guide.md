# 必读

#### 下载: [下载链接(密码6666)](https://wwos.lanzouj.com/ioVOc117nsqh)

#### QQ频道: 24oyp5x92q

#### QQ群: 206490280 

#### 感谢贡献: [https://github.com/tkkcc/ArkLights](https://github.com/tkkcc/ArkLights)（一款明日方舟自动化工具）👍

#### 此APP工具源码: [https://gitee.com/boluokk/e7-helper](https://gitee.com/boluokk/e7-helper) （好用就给个star吧, 你的star将会是作者最大的帮助）👍

### 最近更新: 
- 修复讨伐,祭坛过度点击问题

### 设备兼容

> 1. 需要root权限，推荐使用蓝叠模拟器或者雷电模拟器（国服没有开root检测）
> 2. 分辨率：720x1280 或 1280x720(更新到最新才能)
> 3. DPI：320

### 模拟器下载地址

> - 雷电模拟器：[https://www.ldmnq.com/](https://www.ldmnq.com/)
> - 蓝叠模拟器：[https://www.bluestacks.cn/](https://www.bluestacks.cn/)
> - MUMU模拟器：[https://mumu.163.com/](https://mumu.163.com/)

### 模拟器详细设置📺

#### 蓝叠（国际版）

> 1. 开启root权限，在模拟器安装路径下，有一个名字为   <font color="skyblue">bluestacks.conf</font> 的文件，用记事本打开将文件中的 bst.instance.Pie64.enable_root_access=" <font color="red">0</font>"  修改成  bst.instance.Pie64.enable_root_access=" <font color="red">1</font>" 保存并重启模拟器（bst.instance.Pie64.enable_root_access可能叫其他的，但是带有<font color='red'>root_access</font>就是了）
> 2. 在模拟器设置中，显示选项中修改分辨率和DPI、并且开启ROOT

#### 雷电

> 1. 雷电模拟器、蓝叠国内版、MUMU6...设置都差不多
> 2. 在模拟器设置中，显示选项中修改分辨率和DPI、并且开启ROOT

### 如何使用?🦊

1. #### 怎么刷书签?🍃

   > 点击 [开始刷书签] 并且设置好次数就行了。
   
### 常见问题?😀
1. #### 模拟器玩E7很卡?
   > 可以通过修改程序优先级: [https://www.ldmnq.com/forum/45337.html](https://www.ldmnq.com/forum/45337.html)
2. #### 为什么我刷标签会在主界面乱点或者没反应?
   > 在脚本中选中主题为默认的
3. #### 软件第一次打开为什么一直在等待root开启?
   > 在模拟器设置中将root开启(找不到, 百度xx模拟器怎么开启root)
4. #### 为什么脚本点击开始会弹出背包设置?
   > 脚本中部分功能运行中会需要清理背包: 收取宠物、刷图
---
home: true
heroImage: /logo.jpg
heroText: E7Helper
tagline: 一款第七史诗自动化工具
actionText: 开始使用  👉
actionLink: /zh/guide/
features:
- title: 开源免费
  details: 开源免费, 持续更新。
- title: 极小容量
  details: 全代码文件24KB大小, 极致缩小文件大小。
- title: 热更新
  details: 无需重复安装APK文件，在线热更新。
- title: 功能覆盖
  details: 讨伐、精灵祭坛、每日、刷标签、刷JJC。
footer: Apache-2.0 Licensed | Copyright © 🍍
---